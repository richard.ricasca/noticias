const notesCtrl = {};

// Models
const Note = require("../models/Note");

notesCtrl.renderNoteForm = (req, res) => {
  res.render("notes/new-note");
};

notesCtrl.createNewNote = async (req, res) => {
  const {categoria, title, description,autor,comentario } = req.body;
  const errors = [];
  if (!title) {
    errors.push({ text: "Por favor escriba un titulo." });
  }
  if (!description) {
    errors.push({ text: "Por favor escriba un contenido" });
  }
  if (errors.length > 0) {
    res.render("notes/new-note", {
      errors,
      title,
      description,
      autor,
      comentario
    });
  } else {
    const newNote = new Note({categoria, title, description,autor,comentario });
    newNote.user = req.user.id;
    await newNote.save();
    req.flash("success_msg", "Noticia publicada satisfactoriamente");
    res.redirect("/news_listar");
  }
};

notesCtrl.renderNotes = async (req, res) => {
  const notes = await Note.find({ user: req.user.id })
    .sort({ date: "desc" })
    .lean();
  res.render("notes/all-notes", { notes });
};

notesCtrl.renderEditForm = async (req, res) => {
  const note = await Note.findById(req.params.id).lean();
  if (note.user != req.user.id) {
    req.flash("error_msg", "Not Authorized");
    return res.redirect("/notes");
  }
  res.render("notes/edit-note", { note });
};

notesCtrl.updateNote = async (req, res) => {
  const {categoria, title, description,autor,comentario } = req.body;
  await Note.findByIdAndUpdate(req.params.id, { categoria,title, description,autor,comentario });
  req.flash("success_msg", "Se actualizo satisfactoriamente");
  res.redirect("/news_listar");
};

notesCtrl.deleteNote = async (req, res) => {
  await Note.findByIdAndDelete(req.params.id);
  req.flash("success_msg", "Se elimino satisfactoriamente");
  res.redirect("/news_listar");
};

module.exports = notesCtrl;

const express = require("express");
const router = express.Router();

// Controller
const {
  renderNoteForm,
  createNewNote,
  renderNotes,
  renderEditForm,
  updateNote,
  deleteNote
} = require("../controllers/notes.controller");

// Helpers
const { isAuthenticated } = require("../helpers/auth");

// New Note
router.get("/news_crear", renderNoteForm);

router.post("/notes/new-note", createNewNote);

// Get All Notes
router.get("/news_listar",  renderNotes);

// Edit Notes
router.get("/news_update/:id", renderEditForm);

router.put("/notes/edit-note/:id", updateNote);

// Delete Notes
router.delete("/notes/delete/:id", deleteNote);

module.exports = router;

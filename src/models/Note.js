const { Schema, model } = require("mongoose");

const NoteSchema = new Schema(
  {
    categoria: {
      type: String,
      required: true
    },
    title: {
      type: String,
      required: true
    },
    description: {
      type: String,
      required: true
    },
    autor: {
      type: String,
      required: true
    },
    comentario: {
      type: String,
      required: true
    },
    user: {
      type: String,
      required: true
    }
  },
  {
    timestamps: true
  }
);

module.exports = model("Note", NoteSchema);
